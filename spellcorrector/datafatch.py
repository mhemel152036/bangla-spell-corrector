'''
Created on Sep 9, 2019

@author: HEMEL
'''

#!/usr/bin/python3
# -- coding: utf-8 --

class DataFatch:
    def __init__(self,data_dir,file_name):
        self.data_dir=data_dir
        self.file_name=file_name
        
    def readtext(self):
        #data_dir = "../resources/train_data/"
        file_to_open = self.data_dir + self.file_name #specify file location
        
        try:
            f=(open(file_to_open,'r',encoding = 'utf-8'))
              
        except FileNotFoundError:
            print("No such file or directory")
#         finally:
#             print("File path error")
            
        return f.read()