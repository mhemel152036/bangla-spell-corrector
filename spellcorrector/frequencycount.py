# -*- coding: utf-8 -*-
'''
Created on Sep 8, 2019

@author: HEMEL
'''

import collections
class FrequencyCount:
    
    def __init__(self):
        pass
    def words(self,text):
        word_list=text.split() #generate word list
        #print(word_list)
        return word_list
    def train(self,features):
        model = collections.defaultdict(lambda: 1) #set default key value 1
        for f in features:
            model[f]+=1 #increment key value by 1
        #print(len(model))
        return model