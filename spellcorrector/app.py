'''
Created on Oct 9, 2019

@author: HEMEL
'''

#!/usr/bin/python3
# -- coding: utf-8 --

import sys
from PyQt5.Qt import *
from PyQt5.QtWidgets import *
from spellcorrector.correction import Correction
from spellcorrector.datafatch import DataFatch

class window(QMainWindow):
    
    def __init__(self):
        super().__init__()
        self.title="Simple Gui window"
        self.left=100
        self.top=50
        self.height=450
        self.width=500
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        
        
        self.createMenu()
        
    
        self.textbox = QTextEdit(self)
#         self.textbox.setPlainText("জমাট ব্যাটিং শুরু করেছিলেন কাল খেলার প্রথম দিনেই..\n")
        self.textbox.move(20, 50)
        self.textbox.resize(460,300)
        
        
        
        self.button = QPushButton('Click here', self)
        self.button.setToolTip('Click here to correct text')
        self.button.move(220,355)
        self.button.clicked.connect(self.on_click)

        
        self.show()
    
    
    @pyqtSlot()
    def on_click(self):
        #bangla alphabet list
        bn_alphabet="অআইঈউঊঋএঐওঔকখগঘঙচছজঝঞটঠডঢণটথদধনপফবভমযরলশষসহড়ঢ়য়ৎংঃঁ া ি ী ু ূ ৃ ে ৈ ো ৌ ্যর্চচ্র ্‌ ।"
        #Test data location
        df=DataFatch("../resources/input_data/","input.txt") 
        #create and object of Correction calls
        corr=Correction(bn_alphabet)
         
        input_text= df.readtext()
         
#         correct_text=''
        for word in input_text.split():
            #print (f'{corr.correct(word)} ')
            self.textbox.insertPlainText(f'{corr.correct(word)} ')
#             correct_text+=f'{corr.correct(word)}'
            
#         self.textbox.insertPlainText(correct_text)
             
#         print(correct_text)
        
        
        
    
    #Create Men 
    def createMenu(self):
        mainMenu=self.menuBar()
        fileMenu=mainMenu.addMenu('File')
        editMenu=mainMenu.addMenu('Edit')
        viewMenu=mainMenu.addMenu("View")
        toolMenu=mainMenu.addMenu('Tool')
        helpMenu=mainMenu.addMenu('Help')
        
        exitButton=QAction('Exit',self)
        exitButton.setShortcut('Crtl+Q')
        exitButton.setStatusTip("Exit application")
        exitButton.triggered.connect(self.close)
        fileMenu.addAction(exitButton)
