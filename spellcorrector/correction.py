# -*- coding: utf-8 -*-
'''
Created on Sep 8, 2019

@author: HEMEL
'''
from spellcorrector.datafatch import DataFatch
from spellcorrector.frequencycount import FrequencyCount

class Correction:
    
    def __init__(self,alphabet):
        self.alphabet=alphabet
        
    #Train data location
    df=DataFatch("../resources/train_data/","big.txt")
    fc=FrequencyCount()
    
    word_frequency_list = fc.train(fc.words(df.readtext()))
    
    #alphabet="অআইঈউঊঋএঐওঔকখগঘঙচছজঝঞটঠডঢণটথদধনপফবভমযরলশষসহড়ঢ়য়ৎংঃঁজ া ি ী ু ূ ৃ ে ৈ ো ৌ ্য । ্‌"
      
    def edits1(self,word):
        splits     = [(word[:i], word[i:]) for i in range(len(word) + 1)]
        #print (splits)
        deletes    = [a + b[1:] for a, b in splits if b]
        #print (deletes)
        transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b)>1]
        #print (transposes)
        replaces   = [a + c + b[1:] for a, b in splits for c in self.alphabet if b]
        #print (replaces)
        inserts    = [a + c + b     for a, b in splits for c in self.alphabet]
        #print (inserts)
        ret = set(deletes + transposes + replaces + inserts)
        #print (len(ret))
        return ret
      
    def known_edits2(self,word):
        return set(e2 for e1 in self.edits1(word) for e2 in self.edits1(e1) if e2 in self.word_frequency_list)
      
    def known(self,words): return set(w for w in words if w in self.word_frequency_list)
      
    # return most possible correct word
    def correct(self,word):
        candidates = self.known([word]) or self.known(self.edits1(word)) or self.known_edits2(word) or [word]
        return max(candidates, key=self.word_frequency_list.get)