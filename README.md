# Bangla Spell Corrector

**Bangla Spell Corrector** is based on [Peter Norvig's](https://norvig.com/spell-correct.html "Peter Norvig's")     blog post on setting up a simple spell checking algorithm.     
**Bangla spell corrector** is a simple spell checking program intended to correct simple Bangla spelling mistake quickly enough. It is a program which is written with Python programming language. It is developed as an additional tool of our main project     **Bangla Speech Recognition system** intending to maximize the accuracy of recognized voice into text transcription. Since it is developed for the existing model, I have used model-specific Bangla data to train it and got quite satisfactory result.

### Prerequisites
Things you need to run the system

- Python 3.7
- Python Ide (e.g. Spyder, PyDev, PyCharm)
- Sample Data for training and testing purpose

## Built With

- [Pytho 3.7](https://www.python.org/downloads/release/python-370/) - The programming language used
- [PyDev](https://www.pydev.org/) - The Ide used


## Author

* **Toufique Hasan Hemel** - [Bangla Spell Corrector](https://mhemel152036@bitbucket.org/mhemel152036/bangla-spell-corrector.git)


## Credit

- [pyspellchecker](https://github.com/barrust/pyspellchecker.git) python spell checking library
- [Peter Norvig](https://norvig.com/spell-correct.html) blog post on setting up a simple spell checking algorithm
- [NASa nasacj](https://github.com/nasacj/spell-corrector.git) Spell Corrector Implemented in C++


## License & Coypyright

Copyright (c) 2019, Toufique Hasan Hemel
This project is licensed under the [MIT License](LICENSE) 